﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CheckersBoard : MonoBehaviour
{
    public int AiLevel = 2;
    public GameObject WhitePiecePrefab;
    public GameObject BlackPiecePrefab;
    public bool IsWhite;

    private int EvaluateCounter = 0;

    public Piece[,] Pieces = new Piece[8, 8];

    private readonly Vector3 _boardOffset = new Vector3(-4.0f, 0, -4.0f);
    private readonly Vector3 _pieceOffset = new Vector3(0.5f, 0, 0.5f);

    private bool _isWhiteTurn;
    private bool _hasKilled;

    private Piece _selectedPiece;
    private List<Piece> _forcedPieces;

    private Vector2 _mouseOver;
    private Vector2 _startDrag;
    private Vector2 _endDrag;

    private readonly List<int[]> _possibleMoves = new List<int[]>();

    private bool _victory = false;

    private CheckersBoard()
    {
        _possibleMoves.Add(new int[] { -1, 1 }); // Влево вверх
        _possibleMoves.Add(new int[] { 1, 1 }); // Вправо вверх
        _possibleMoves.Add(new int[] { -1, -1 }); // Влево вниз
        _possibleMoves.Add(new int[] { 1, -1 }); // Вправо вниз
    }

    private void Start()
    {
        _isWhiteTurn = true;
        IsWhite = true;
        _forcedPieces = new List<Piece>();
        GenerateBoard();
    }

    private void Update()
    {
        UpdateMouseOver();

        if ((IsWhite) ? _isWhiteTurn : !_isWhiteTurn)
        {
            int x = (int)_mouseOver.x;
            int y = (int)_mouseOver.y;

            if (_selectedPiece != null)
                UpdatePieceDrag(_selectedPiece);

            if (Input.GetMouseButtonDown(0))
                SelectPiece(x, y);

            if (Input.GetMouseButtonUp(0))
                TryMove((int)_startDrag.x, (int)_startDrag.y, x, y);
        }
    }
    private void UpdateMouseOver()
    {
        if (!Camera.main)
        {
            Debug.Log("Unable to find main camera");
            return;
        }

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25.0f, LayerMask.GetMask("Board")))
        {
            _mouseOver.x = (int)(hit.point.x - _boardOffset.x);
            _mouseOver.y = (int)(hit.point.z - _boardOffset.z);
        }
        else
        {
            _mouseOver.x = -1;
            _mouseOver.y = -1;
        }
    }
    private void UpdatePieceDrag(Piece p)
    {
        if (!Camera.main)
        {
            Debug.Log("Unable to find main camera");
            return;
        }

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25.0f, LayerMask.GetMask("Board")))
        {
            p.transform.position = hit.point + Vector3.up;
        }

    }

    private void SelectPiece(int x, int y)
    {
        // Out of bounds
        if (x < 0 || x >= 8 || y < 0 || y >= 8)
            return;

        Piece p = Pieces[x, y];
        if (p != null && p.isWhite == IsWhite)
        {
            if (_forcedPieces.Count == 0)
            {
                _selectedPiece = p;
                _startDrag = _mouseOver;
            }
            else
            {
                // Look for piece under our forced pieces list
                if (_forcedPieces.Find(fp => fp == p) == null)
                    return;
                _selectedPiece = p;
                _startDrag = _mouseOver;
            }
        }
    }
    private void TryMove(int x1, int y1, int x2, int y2)
    {
        _forcedPieces = ScanForPossibleMove();

        _startDrag = new Vector2(x1, y1);
        _endDrag = new Vector2(x2, y2);
        _selectedPiece = Pieces[x1, y1];

        // Упс. Вышли за пределы
        if (x2 < 0 || x2 >= 8 || y2 < 0 || y2 >= 8)
        {
            if (_selectedPiece != null)
                MovePiece(_selectedPiece, x1, y1);

            _startDrag = Vector2.zero;
            _selectedPiece = null;
            return;
        }

        if (_selectedPiece != null)
        {
            // Если хода не было
            if (_endDrag == _startDrag)
            {
                MovePiece(_selectedPiece, x1, y1);
                _startDrag = Vector2.zero;
                _selectedPiece = null;
                return;
            }

            // Правильно ли ходим?
            if (_selectedPiece.ValidMove(Pieces, x1, y1, x2, y2))
            {
                // Мы рубим?
                // Перепрыгиваем?
                if (Mathf.Abs(x2 - x1) == 2)
                {
                    Piece p = Pieces[(x1 + x2) / 2, (y1 + y2) / 2];
                    if (p != null)
                    {
                        Pieces[(x1 + x2) / 2, (y1 + y2) / 2] = null;
                        Destroy(p.gameObject);
                        _hasKilled = true;
                    }
                }

                // Должны ли мы ещё рубить?
                if (_forcedPieces.Count != 0 && !_hasKilled)
                {
                    MovePiece(_selectedPiece, x1, y1);
                    _startDrag = Vector2.zero;
                    _selectedPiece = null;
                    return;
                }

                Pieces[x2, y2] = _selectedPiece;
                Pieces[x1, y1] = null;
                MovePiece(_selectedPiece, x2, y2);

                EndTurn();
            }
            else
            {
                MovePiece(_selectedPiece, x1, y1);
                _startDrag = Vector2.zero;
                _selectedPiece = null;
                return;
            }
        }
    }
    private void EndTurn()
    {
        int x = (int)_endDrag.x;
        int y = (int)_endDrag.y;

        // Promotions
        if (_selectedPiece != null)
        {
            if (_selectedPiece.isWhite && !_selectedPiece.isKing && y == 7)
            {
                _selectedPiece.isKing = true;
                _selectedPiece.transform.Rotate(Vector3.right * 180);
            }
            else if (!_selectedPiece.isWhite && !_selectedPiece.isKing && y == 0)
            {
                _selectedPiece.isKing = true;
                _selectedPiece.transform.Rotate(Vector3.right * 180);
            }
        }

        _selectedPiece = null;
        _startDrag = Vector2.zero;

        if (ScanForPossibleMove(x, y).Count != 0 && _hasKilled)
            return;

        _isWhiteTurn = !_isWhiteTurn;
        IsWhite= !IsWhite;
        _hasKilled = false;
        CheckVictory();

        if (!_isWhiteTurn && !_victory)
        {
            AlphaBeta(false, AiLevel, -500, 500, null);
            //RunAlphaBeta(false, 0, -500, 500, null);
        }
    }
    private void CheckVictory()
    {
        var ps = FindObjectsOfType<Piece>();
        bool hasWhite = false, hasBlack = false;
        for (int i = 0; i < ps.Length; i++)
        {
            if (ps[i].isWhite)
                hasWhite = true;
            else
                hasBlack = true;
        }

        if (!hasWhite)
            Victory(false);
        if (!hasBlack)
            Victory(true);
    }
    private void Victory(bool isWhite)
    {
        _victory = true;
        if (isWhite)
            Debug.Log("White team has won");
        else
            Debug.Log("Black team has won");
    }
    private List<Piece> ScanForPossibleMove(int x, int y)
    {
        _forcedPieces = new List<Piece>();

        if (Pieces[x, y].IsForceToMove(Pieces, x, y))
            _forcedPieces.Add(Pieces[x, y]);

        return _forcedPieces;
    }
    private List<Piece> ScanForPossibleMove()
    {
        _forcedPieces = new List<Piece>();

        // Check all pieces
        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++)
                if (Pieces[i, j] != null && Pieces[i, j].isWhite == _isWhiteTurn)
                    if (Pieces[i, j].IsForceToMove(Pieces, i, j))
                        _forcedPieces.Add(Pieces[i, j]);
        return _forcedPieces;
    }
    private List<int[]> ScanForPossibleAiMove(bool isWhiteTurn)
    {
        var moves = new List<int[]>();

        // Check all pieces
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (Pieces[i, j] != null && Pieces[i, j].isWhite == isWhiteTurn)
                    if (Pieces[i, j].IsForceToMove(Pieces, i, j))
                        moves.Add(new int[] { i, j });
            }
        }
        
        return moves;
    }

    private void GenerateBoard()
    {
        // Generate white team
        for (int y = 0; y < 3; y++)
        {
            bool oddRow = (y % 2 == 0);
            for (int x = 0; x < 8; x += 2)
            {
                // Generate out piece
                GeneratePiece(oddRow ? x : x + 1, y);
            }
        }

        // Generate black team
        for (int y = 7; y > 4; y--)
        {
            bool oddRow = (y % 2 == 0);
            for (int x = 0; x < 8; x += 2)
            {
                // Generate out piece
                GeneratePiece(oddRow ? x : x + 1, y);
            }
        }
    }
    private void GeneratePiece(int x, int y)
    {
        bool isPieceWhite = (y > 3) ? false : true;
        GameObject go = Instantiate(isPieceWhite ? WhitePiecePrefab : BlackPiecePrefab) as GameObject;
        go.transform.SetParent(transform);
        Piece p = go.GetComponent<Piece>();
        Pieces[x, y] = p;
        MovePiece(p, x, y);
    }
    private void MovePiece(Piece p, int x, int y)
    {
        p.transform.position = (Vector3.right * x) + (Vector3.forward * y) + _boardOffset + _pieceOffset;
    }

    // AI

    /// <summary>
    /// Возвращает доступные ходы для шашки
    /// </summary>
    /// <param name="x">Координата x шашки которой ходим</param>
    /// <param name="y">Координата y шашки которой ходим</param>
    /// <returns>Массив с возможными ходами</returns>
    private List<int[]> AvailableMoves(int x, int y)
    {
        Piece selectedPiece = Pieces[x, y];
        List<int[]> moves = new List<int[]>();
        bool isHaveCutPieces = false;

        for (int i = 0; i < _possibleMoves.Count; i++)
        {
            int x2 = x + _possibleMoves[i][0];
            int y2 = y + _possibleMoves[i][1];
            int x3 = x + _possibleMoves[i][0] * 2;
            int y3 = y + _possibleMoves[i][1] * 2;

            if ((x3 < 8 && y3 < 8 && x3 >= 0 && y3 >= 0) && selectedPiece.ValidMove(Pieces, x, y, x3, y3))
            {
                if (!isHaveCutPieces)
                {
                    moves.Clear();
                }

                moves.Add(new int[] { x3, y3 });
                isHaveCutPieces = true;
            }

            if (!isHaveCutPieces && (x2 < 8 && y2 < 8 && x2 >= 0 && y2 >= 0)
                                 && selectedPiece.ValidMove(Pieces, x, y, x2, y2))
            {
                moves.Add(new int[] { x2, y2 });
            }

        }

        return moves;
    }

    /// <summary>
    /// Эмулирует ход ИИ
    /// </summary>
    /// <param name="x">Координата x шашки которой ходим</param>
    /// <param name="y">Координата y шашки которой ходим</param>
    /// <param name="x2">Координата x куда ходим</param>
    /// <param name="y2">Координата y куда ходим</param>
    /// <param name="isBack">True, если восстанавливаем ход</param>
    /// <param name="isKing">True, если король</param>
    private void EmulateMove(int x, int y, int x2, int y2, bool isBack, bool isKing)
    {
        var selectedPiece = Pieces[x, y];

        // Чики-брики и в дамки
        if (selectedPiece.isWhite && y2 == 7)
        {
            selectedPiece.isKing = true;
        }
        else if (!selectedPiece.isWhite && y2 == 0)
        {
            selectedPiece.isKing = true;
        }

        if (isBack)
        {
            selectedPiece.isKing = isKing;
        }

        // Мы рубим?
        // Перепрыгиваем?
        if (Mathf.Abs(x2 - x) == 2)
        {
            var p = Pieces[(x + x2) / 2, (y + y2) / 2];
            if (p != null)
            {
                p.gameObject.SetActive(isBack);
            }
        }

        Pieces[x2, y2] = selectedPiece;
        Pieces[x, y] = null;
        MovePiece(selectedPiece, x2, y2);
    }

    /// <summary>
    /// Оценивает текущее состояние доски
    /// </summary>
    /// <returns>Возвращает оценку</returns>
    private int Evaluate()
    {
        int evaluateWhite = 0;
        int evaluateBlack = 0;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                Piece piece = Pieces[i, j];
                if (piece != null && piece.gameObject.activeSelf)
                {
                    if (piece.isWhite)
                    {
                        evaluateWhite += 100;
                        if (piece.isKing)
                        {
                            evaluateWhite += 200;
                        }
                    }
                    else
                    {
                        evaluateBlack += 100;
                        if (piece.isKing)
                        {
                            evaluateBlack += 200;
                        }
                    }
                }
            }
        }
        
        return evaluateBlack - evaluateWhite;
    }

    /// <summary>
    /// Оценивает текущее состояние доски
    /// </summary>
    /// <param name="isWhite">Цвет для которого получаем оценку</param>
    /// <returns>Возвращает оценку</returns>
    private int Evaluate(bool isWhite)
    {
        int evaluateWhite = 0;
        int evaluateBlack = 0;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                Piece piece = Pieces[i, j];
                if (piece != null && piece.gameObject.activeSelf)
                {
                    if (piece.isWhite)
                    {
                        evaluateWhite += 100;
                        if (piece.isKing)
                        {
                            evaluateWhite += 200;
                        }
                    }
                    else
                    {
                        evaluateBlack += 100;
                        if (piece.isKing)
                        {
                            evaluateBlack += 200;
                        }
                    }
                }
            }
        }

        return isWhite ? evaluateWhite : evaluateBlack;
    }

    private List<int[]> GetPiecesForSearchBestMove(bool isWhite)
    {
        var needCut = ScanForPossibleAiMove(isWhite);
        var pieces = new List<int[]>();

        if (needCut.Count != 0)
        {
            foreach (var pieceCoord in needCut)
            {
                var piece = Pieces[pieceCoord[0], pieceCoord[1]];
                if (piece.isWhite != isWhite) continue;

                pieces.Add(new int[2] { pieceCoord[0], pieceCoord[1] });
            }
        }
        else
        {
            for (var x = 0; x < 8; x++)
            {
                for (var y = 0; y < 8; y++)
                {
                    var piece = Pieces[x, y];
                    if (piece == null) continue;
                    if (piece.isWhite != isWhite) continue;

                    pieces.Add(new int[2] { x, y });
                }
            }
        }

        return pieces;
    }

    private struct moveStruct
    {
        public int value;
        public List<int[]> moves;

        public moveStruct(int _value, List<int[]> _moves)
        {
            value = _value;
            moves = _moves;
        }

    };

    private moveStruct RunAlphaBeta(bool isWhite, int recursiveLevel, int alpha, int beta, List<int[]> needMove)
    {
        // На последнем уровне дерева (на листах) возвращаем значение функции оценки, или если есть шашки с вынужденым ходом
        if (recursiveLevel >= this.AiLevel && (needMove == null || needMove.Count == 0))
            return new moveStruct(Evaluate(), new List<int[]>());

        // Лучший ход
        int[] bestMove = null;
        int minMax = isWhite ? 0 : 24;
        var pieces = needMove != null && needMove.Count != 0 ? needMove : GetPiecesForSearchBestMove(isWhite);
        var bestMoves = new List<int[]>();

        foreach (var pieceCoords in pieces)
        {
            List<int[]> moves = AvailableMoves(pieceCoords[0], pieceCoords[1]);

            foreach (var move in moves)
            {
                var test = new moveStruct(0, new List<int[]>());
                List<int[]> cutMove = new List<int[]>();
                bool isKing = Pieces[pieceCoords[0], pieceCoords[1]].isKing;

                EmulateMove(pieceCoords[0], pieceCoords[1], move[0], move[1], false, isKing);
                if (ScanForPossibleMove(move[0], move[1]).Count != 0)
                {
                    cutMove.Add(new int[] { move[0], move[1] });

                }
                // Оцениваем, насколько хорош ход, который мы выбрали
                test = RunAlphaBeta(cutMove.Count == 0 ? !isWhite : isWhite, recursiveLevel + 1, alpha, beta, cutMove);

                // Восстанавливаем исходное состояние
                EmulateMove(move[0], move[1], pieceCoords[0], pieceCoords[1], true, isKing);

                // Если он лучше всех, что были до этого - запомним, что он лучший
                if ((test.value > minMax && !isWhite) || (test.value <= minMax && isWhite) || (bestMove == null))
                {
                    minMax = test.value;
                    bestMoves = Mathf.Abs(pieceCoords[0] - move[0]) == 2
                                && test.moves.TrueForAll(value => Pieces[value[0], value[1]] == isWhite) ? test.moves : new List<int[]>();
                    bestMove = new int[4] { pieceCoords[0], pieceCoords[1], move[0], move[1] };
                }

                if (isWhite)
                    beta = Mathf.Min(beta, test.value);
                else
                    alpha = Mathf.Max(alpha, test.value);

                if (beta < alpha)
                    break;
            }
        }

        if (bestMove == null)
            return new moveStruct(Evaluate(), bestMoves);

        bestMoves.Add(new int[] { bestMove[0], bestMove[1], bestMove[2], bestMove[3] });

        // Ходим
        if (recursiveLevel == 0)
        {
            bestMoves.Reverse();
            foreach (var pieceMove in bestMoves)
            {
                TryMove(pieceMove[0], pieceMove[1], pieceMove[2], pieceMove[3]);
            }
        }

        return new moveStruct(minMax, bestMoves);
    }

    private moveStruct AlphaBeta(bool isWhite, int depth, int alpha, int beta, List<int[]> needMove)
    {
        // На последнем уровне дерева (на листах) возвращаем значение функции оценки, или если есть шашки с вынужденым ходом
        if (depth <= 0 && (needMove == null || needMove.Count == 0))
            return new moveStruct(Evaluate(isWhite), new List<int[]>());

        // Лучший ход
        int[] bestMove = null;
        var pieces = needMove != null && needMove.Count != 0 ? needMove : GetPiecesForSearchBestMove(isWhite);
        var bestMoves = new List<int[]>();

        foreach (var pieceCoords in pieces)
        {
            List<int[]> moves = AvailableMoves(pieceCoords[0], pieceCoords[1]);

            foreach (var move in moves)
            {
                List<int[]> cutMove = new List<int[]>();
                bool isKing = Pieces[pieceCoords[0], pieceCoords[1]].isKing;

                EmulateMove(pieceCoords[0], pieceCoords[1], move[0], move[1], false, isKing);
                if (ScanForPossibleMove(move[0], move[1]).Count != 0)
                {
                    cutMove.Add(new int[] { move[0], move[1] });

                }
                // Оцениваем, насколько хорош ход, который мы выбрали
                var test = AlphaBeta(cutMove.Count == 0 ? !isWhite : isWhite, depth - 1, -beta, -alpha, cutMove);
                test.value = -test.value;
                // Восстанавливаем исходное состояние
                EmulateMove(move[0], move[1], pieceCoords[0], pieceCoords[1], true, isKing);

                // Если он лучше всех, что были до этого - запомним, что он лучший
                if ((test.value > alpha) || (bestMove == null))
                {
                    bestMoves = Mathf.Abs(pieceCoords[0] - move[0]) == 2
                                && test.moves.TrueForAll(value => Pieces[value[0], value[1]] == isWhite) ? test.moves : new List<int[]>();
                    bestMove = new int[4] { pieceCoords[0], pieceCoords[1], move[0], move[1] };
                }

                if (alpha < beta)
                    break;
            }
        }

        // Если некуда больше ходить
        if (bestMove == null)
        {
            // Достигли терминального состояния
            if (depth == AiLevel)
            {
                Victory(!isWhite);
            }
            return new moveStruct(Evaluate(isWhite), bestMoves);
        }

        bestMoves.Add(new int[] { bestMove[0], bestMove[1], bestMove[2], bestMove[3] });

        // Ходим
        if (depth == AiLevel)
        {
            bestMoves.Reverse();
            foreach (var pieceMove in bestMoves)
            {
                TryMove(pieceMove[0], pieceMove[1], pieceMove[2], pieceMove[3]);
            }
        }

        return new moveStruct(alpha, bestMoves);
    }
}
