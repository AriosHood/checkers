﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour
{
    public bool isWhite;
    public bool isKing;

    /// <summary>
    /// Проверяет есть ли шашки для рубки
    /// </summary>
    /// <param name="board">Доска - вирутальное расположение шашек</param>
    /// <param name="x">Координата x где шашка была взята</param>
    /// <param name="y">Координата y где шашка была взята</param>
    /// <returns>Возвращает true, если не все шашки срублены</returns>
    public bool IsForceToMove(Piece[,] board, int x, int y)
    {
        if (isWhite || isKing)
        {
            // Top left
            if (x >= 2 && y <= 5)
            {
                Piece p = board[x - 1, y + 1];
                // If there is a piece, and it is not the same color as ours
                if (p != null && p.gameObject.activeSelf && p.isWhite != isWhite)
                {
                    // Check  if its possible to land after the jump
                    if (board[x - 2, y + 2] == null)
                        return true;
                }
            }

            // Top right
            if (x <= 5 && y <= 5)
            {
                Piece p = board[x + 1, y + 1];
                // If there is a piece, and it is not the same color as ours
                if (p != null && p.gameObject.activeSelf && p.isWhite != isWhite)
                {
                    // Check  if its possible to land after the jump
                    if (board[x + 2, y + 2] == null)
                        return true;
                }
            }
        }

        if (!isWhite || isKing)
        {

            // Bottom left
            if (x >= 2 && y >= 2)
            {
                Piece p = board[x - 1, y - 1];
                // If there is a piece, and it is not the same color as ours
                if (p != null && p.gameObject.activeSelf && p.isWhite != isWhite)
                {
                    // Check  if its possible to land after the jump
                    if (board[x - 2, y - 2] == null)
                        return true;
                }
            }

            // Bottom right
            if (x <= 5 && y >= 2)
            {
                Piece p = board[x + 1, y - 1];
                // If there is a piece, and it is not the same color as ours
                if (p != null && p.gameObject.activeSelf && p.isWhite != isWhite)
                {
                    // Check  if its possible to land after the jump
                    if (board[x + 2, y - 2] == null)
                        return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// Проверка на правильность хода
    /// </summary>
    /// <param name="board">Доска - вирутальное расположение шашек</param>
    /// <param name="x1">Координата x где шашка была взята</param>
    /// <param name="y1">Координата y где шашка была взята</param>
    /// <param name="x2">Координата x куда шашка была поставлена</param>
    /// <param name="y2">Координата y куда шашка была поставлена</param>
    /// <returns>Возвращает true если ход возможен</returns>
    public bool ValidMove(Piece[,] board, int x1, int y1, int x2, int y2)
    {
        // TODO проверить, если фигура деактивна
        Piece move = board[x2, y2];
        // Если новое место не пустое
        if (move != null)
            return false;

        int deltaMove = (int)Mathf.Abs(x1 - x2);
        int deltaMoveY = y2 - y1;

        if (isWhite || isKing)
        {
            if (deltaMove == 1)
            {
                if (deltaMoveY == 1)
                    return true;
            }
            // Проверим есть существует ли срубаемая шашка
            else if (deltaMove == 2)
            {
                if (deltaMoveY == 2)
                {
                    Piece p = board[(x1 + x2) / 2, (y1 + y2) / 2];
                    if (p != null && p.gameObject.activeSelf && p.isWhite != isWhite)
                        return true;
                }
            }
        }

        if (!isWhite || isKing)
        {
            if (deltaMove == 1)
            {
                if (deltaMoveY == -1)
                    return true;
            }
            else if (deltaMove == 2)
            {
                if (deltaMoveY == -2)
                {
                    Piece p = board[(x1 + x2) / 2, (y1 + y2) / 2];
                    if (p != null && p.gameObject.activeSelf && p.isWhite != isWhite)
                        return true;
                }
            }
        }

        return false;
    }
}
